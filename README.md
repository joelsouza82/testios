Data:23/04/2018
Teste Resource IT desenvolvido por Joel de Almeida Souza

Criar um aplicativo de consulta a API do GitHub#
 
Criar um aplicativo para consultar a API do GitHub e trazer os repositórios mais populares de Java. Basear-se no mockup fornecido:

![alt text](https://bitbucket.org/joelsouza82/testios/Screen1.png)
![alt text](https://bitbucket.org/joelsouza82/testios/Screen2.png)
 
Deve conter
• Lista de repositórios. Exemplo de chamada na API: https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1
◦ Paginação na tela de lista, com endless scroll / scroll infinito (incrementando o parâmetro page).
◦ Cada repositório deve exibir Nome do repositório, Descrição do Repositório, Nome / Foto do autor, Número de Stars, Número de Forks
◦ Ao tocar em um item, deve levar a lista de Pull Requests do repositório
• Pull Requests de um repositório. Exemplo de chamada na API: https://api.github.com/repos/<criador>/<repositório>/pulls
◦ Cada item da lista deve exibir Nome / Foto do autor do PR, Título do PR, Data do PR e Body do PR
◦ Ao tocar em um item, deve abrir no browser a página do Pull Request em questão
 
 
A solução DEVE conter
• Arquivo .gitignore
• Usar Storyboard e Autolayout
• Gestão de dependências no projeto.
• Framework para Comunicação com API.
• Mapeamento json -> Objeto.
 
Ganha + pontos se conter (Vaga de sênior é obrigatório*)
• Arquitetura MVC*, MVVM, VIPER ou outra
• Testes unitários no projeto *
• Testes funcionais/ Instrumental *
• App Universal , Ipad | Iphone | Landscape | Portrait (Size Classes)*
• Fastlane incorporado na aplicação*
• UICollectionView*
• Cache de Imagens *
• RxSwift
 
Sugestões
• Objetive C ou Swift são aceitos, será bem visto se utilizar ambas, mas recomendamos ter finalidades definidas para cada. Ex, código em swift e apenas a classe de útil em Objective C (ou o inverso).
• O código será submetido a testes, portanto, não deverá apresentar “warnings” ou quebrar em diferentes cenários adversos. Ex: Sem conexão com a internet
• Códigos copiados da internet na integra poderão perder pontos
• O layout é importante, tanto quanto seu fluxo de navegação
 
OBS
A foto do mockup é meramente ilustrativa, esperamos um layout diferente.
 
 
 
