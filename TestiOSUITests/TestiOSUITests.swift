//
//  TestiOSUITests.swift
//  TestiOSUITests
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import XCTest

class TestiOSUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        XCUIApplication().launch()

    }
    
    override func tearDown() {
        super.tearDown()
    }
    
}
