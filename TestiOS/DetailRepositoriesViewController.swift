//
//  DetailRepositoriesViewController.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import UIKit

class DetailRepositoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

  var repositories : Repositories?
  var dataPullList = [PullRequest]()
    
  var numOpenPull = 0
  var numClosePull = 0
  
  @IBOutlet weak var tableView: UITableView!
  
  
  override func viewWillAppear(_ animated: Bool) {
    
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationItem.title = repositories?.name
    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    
    tableView.dataSource = self
    tableView.delegate   = self
    tableView.tableFooterView = UIView()
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 150.0
    
    if let repositories = repositories{
        PullRequestAPI.getPullRequestListByRepositoriesName(repositoriesName: repositories.name!, ownerName: (repositories.owner?.login)!) { (pullList, numOpened, numClosed) in
        if let pullList = pullList {
          self.dataPullList = pullList
          self.numOpenPull = numOpened
          self.numClosePull = numClosed
          self.tableView.reloadData()
        }
      }
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataPullList.count
  }
    
  // MARK: - TableViewDataSource
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! PullRequestTableViewCell
    
    cell.loadCell(pullObjective: dataPullList[indexPath.row])
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (dataPullList.count > 0) {
            return 40.0
        }
        return 80.0
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if (dataPullList.count > 0) {
      let header = tableView.dequeueReusableCell(withIdentifier: "headerPullDetail") as! HeaderPullDetailTViewController
      header.num_opened.text = "\(numOpenPull.toString()) opened"
      header.num_closed.text = "/ \(numClosePull.toString()) closed"
      header.backgroundColor = UIColor.white
      return header
    }
    let header = tableView.dequeueReusableCell(withIdentifier: "loadHeader") as! LoadHeaderTableViewController
    return header
  }
  
 
  
  // MARK: - TableViewDelegate
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let pullSelected = dataPullList[indexPath.row]
    if let url = URL(string: pullSelected.htmlUrl!) {
      UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
        
      })
    }
  }

}
