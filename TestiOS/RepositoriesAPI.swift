//
//  RepositoriesAPI.swift
//
//  Created by Joel de Almeida Souza on 23/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import Foundation
import Alamofire

class RepositoriesAPI {
  
  static let apiUrl = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="
  
  static func getPagination(page: Int, completionHandler: @escaping (_ repo: [Repositories]?)->() ) {
    let urlPage = apiUrl.appending("\(page)")
    Alamofire.request(urlPage).responseJSON { (response) in
      var repositories = [Repositories]()
      if let json = response.result.value as? [String:Any]{
        if let items = json["items"] as? [[String:Any]]{
          for item in items{
            repositories.append(Repositories(JSON: item)!)
          }
          completionHandler(repositories)
        }
      }
      completionHandler(nil)
    }
  }
  
}
