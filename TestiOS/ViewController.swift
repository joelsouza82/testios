//
//  ViewController.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet weak var mainTableView: UITableView!
  var dataFeed = [Repositories]()
  var page = 1
    var isLoading = false
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Configuração TableView
    mainTableView.delegate = self
    mainTableView.dataSource = self
    mainTableView.tableFooterView = UIView()
    mainTableView.rowHeight = UITableViewAutomaticDimension
    mainTableView.estimatedRowHeight = 150.0
    
    RepositoriesAPI.getPagination(page: page) { (repoModels) in
      if let repoModels = repoModels{
        self.dataFeed = repoModels
        self.mainTableView.reloadData()
      }
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  // MARK: - TableViewDataSource
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (dataFeed.count > 0) {
            return nil
        }
        let header = tableView.dequeueReusableCell(withIdentifier: "feedLoadHeader") as! LoadHeaderTableViewController
        return header
    }
    
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "feedCell", for: indexPath) as! FeedRepositoriesTableViewCell
    
    cell.loadCellForData(repositories: dataFeed[indexPath.row])
    
    return cell
  }
    
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataFeed.count
  }
  
  
 
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if (dataFeed.count > 0) {
      return 0.0
    }
    return 80.0
  }
  
  // MARK: - TableViewDelegate
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    performSegue(withIdentifier: "segueRepositorioInfo", sender: dataFeed[indexPath.row])
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
    if (indexPath.row > dataFeed.count-10 && !isLoading) {
      mainTableView.tableFooterView = tableView.dequeueReusableCell(withIdentifier: "feedLoadHeader")
      isLoading = true
      page += 1
      
      // Listar os itens do repositório
      RepositoriesAPI.getPagination(page: page, completionHandler: { (repoList) in
        if let repoList = repoList {
          
          self.dataFeed.append(contentsOf: repoList)
          
          self.mainTableView.tableFooterView = UIView()
          self.mainTableView.reloadData()
          self.isLoading = false
        }
        
      })
    }
    
  }
  
  // MARK: - Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "segueRepositorioInfo" {
      let repo = sender as! Repositories
      let destinationVC = segue.destination as! DetailRepositoriesViewController
      destinationVC.repositories = repo
    }
  }
  
  
}

