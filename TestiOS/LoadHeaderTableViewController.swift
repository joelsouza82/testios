//
//  LoadHeaderTableViewController.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadHeaderTableViewController: UITableViewCell {

  @IBOutlet weak var loaderView: NVActivityIndicatorView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    loaderView.startAnimating()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func changeStateLoader() {
    if (loaderView.isAnimating){
      loaderView.stopAnimating()
    } else{
      loaderView.startAnimating()
    }
  }
  

}
