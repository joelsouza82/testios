//
//  API.swift
//
//  Created by Joel de Almeida Souza on 23/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import Foundation
import Alamofire

class API {
  
  static let apiURL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1"
  
  static func getApisByPage(page: Int, completionHadler: @escaping ((_ ownersList: [Owner]?)->())){
    Alamofire.request(apiURL).responseJSON { (response) in
    var owners = [Owner]()
      if let json = response.result.value as? [String:Any]{
        let items = json["items"] as! [[String:Any]]
        for item in items{
          if let newOwner = Owner(JSON: item){
            owners.append(newOwner)
          }
        }
        completionHadler(owners)
      }
    }
    completionHadler(nil)
  }
  
}

