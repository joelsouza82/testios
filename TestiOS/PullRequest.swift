//
//  PullRequest.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import Foundation
import ObjectMapper


class PullRequest: Mappable {
  
  var id: Int?
  var url: String?
  var htmlUrl: String?
  var state: String?
  var locked: Bool?
  var title: String?
  var user: Owner?
  var body: String?
  var mergeCommitSha: String?
  var assignee: String?
  var assignees: [String:Any]?
  var createdAt: String?
  var updatedAt: String?
  var closedAt: String?
  var mergedAt: String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    
    id                <- map["id"]
    url                <- map["url"]
    htmlUrl            <- map["html_url"]
    state        	   <- map["state"]
    locked        	   <- map["locked"]
    title        	   <- map["title"]
    user        	   <- map["user"]
    body        	   <- map["body"]
    mergeCommitSha    <- map["merge_commit_sha"]
    assignee         <- map["assignee"]
    assignees        <- map["assignees"]
    createdAt       <- map["created_at"]
    updatedAt       <- map["updated_at"]
    closedAt        <- map["closed_at"]
    mergedAt        <- map["merged_at"]
    
  }
  
}
