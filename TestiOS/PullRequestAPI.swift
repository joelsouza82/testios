//
//  PullRequestAPI.swift
//
//  Created by Joel de Almeida Souza on 23/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import Foundation
import Alamofire

class PullRequestAPI {
  
  static let apiUrlPullRequest = "https://api.github.com/repos"
  
  static func getPullRequestListByRepositoriesName(repositoriesName: String, ownerName: String, completionHandler: @escaping ((_ pullList: [PullRequest]?, _ numOpen: Int, _ numClose: Int)->())) {
    let urlOwner = "\(apiUrlPullRequest)/\(ownerName)/\(repositoriesName)/pulls"
    
    Alamofire.request(urlOwner).responseJSON { (response) in
      var pullList = [PullRequest]()
      var num_opened = 0
      var num_closed = 0
      
      if let json = response.result.value{
        let items = json as! [[String:Any]]
        for item in items{
          let pullReq = PullRequest(JSON: item)!
          
          if pullReq.state == "open"{
            num_opened += 1
          } else{
            num_closed += 1
          }
          
          pullList.append(pullReq)
        }
        completionHandler(pullList, num_opened, num_closed)
      }
      completionHandler(nil, 0, 0)
    }
  }
  
}
