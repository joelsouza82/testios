//
//  Owner.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import Foundation
import ObjectMapper

class Owner: Mappable {
    
  var id: Int?
  var gravatarId: String?
  var siteAdmin: Bool?
  var type: String?
  var login: String?
  var avatarUrl: String?
  var url: String?
  var htmlUrl: String?
  var followerUrl: String?
  var followingUrl: String?
  var gistsUrl: String?
  var starredUrl: String?
  var subscriptionsUrl: String?
  var organizationsUrl: String?
  var reposUrl: String?
  var eventsUrl: String?
  var receivedEventsUrl: String?
  
  

  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
   
    id                  <- map["id"]
    gravatarId         <- map["gravatar_id"]
    siteAdmin          <- map["site_admin"]
    login               <- map["login"]
    type                <- map["type"]
    avatarUrl          <- map["avatar_url"]
    htmlUrl             <- map["html_url"]
    url                 <- map["url"]
    followerUrl         <- map["followers_url"]
    followingUrl       <- map["following_url"]
    gistsUrl           <- map["gists_url"]
    starredUrl         <- map["starred_url"]
    subscriptionsUrl   <- map["subscriptions_url"]
    organizationsUrl   <- map["organizations_url"]
    reposUrl           <- map["repos_url"]
    eventsUrl          <- map["events_url"]
    receivedEventsUrl <- map["received_events_url"]
    
  }
  
}

