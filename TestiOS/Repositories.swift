//
//  Repositories.swift
//
//  Created by Joel de Almeida Souza on 23/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import Foundation
import ObjectMapper


class Repositories: Mappable {
  
  var id: Int?
  var name: String?
  var fullName: String?
  var description: String?
  var owner: Owner?
  var isPrivate: Bool?
  var url: String?
  var htmlUrl: String?
  var fork: Bool?
  var forksUrl: String?
  var forks: Int?
  var forksCount: Int?
  var keysUrl: String?
  var collaboratorsUrl: String?
  var teamsUrl: String?
  var hooksUrl: String?
  var issueEventsUrl: String?
  var eventsUrl: String?
  var assigneesUrl: String?
  var branchesUrl: String?
  var tagsUrl: String?
  var blobsUrl: String?
  var gitTagsUrl: String?
  var gitRefsUrl: String?
  var treesUrl: String?
  var statusesUrl: String?
  var languagesUrl: String?
  var stargazersUrl: String?
  var contributorsUrl: String?
  var subscribersUrl: String?
  var subscriptionUrl: String?
  var commitsUrl: String?
  var gitCommitsUrl: String?
  var commentsUrl: String?
  var issueCommentUrl: String?
  var contentsUrl: String?
  var compareUrl: String?
  var mergesUrl: String?
  var archiveUrl: String?
  var downloadsUrl: String?
  var issuesUrl: String?
  var pullsUrl: String?
  var milestonesUrl: String?
  var notificationsUrl: String?
  var labelsUrl: String?
  var releasesUrl: String?
  var deploymentsUrl: String?
  var gitUrl: String?
  var sshUrl: String?
  var cloneUrl: String?
  var svnUrl: String?
  var homepage: String?
  var size: Int?
  var stargazersCount: Int?
  var watchersCount: Int?
  var language: String?
  var hasIssues: Bool?
  var hasProjects: Bool?
  var hasDownloads: Bool?
  var hasWiki: Bool?
  var hasPages: Bool?
  var watchers: Int?
  var mirrorUrl: String?
  var openIssuesCount: Int?
  var score: Double?
  var openIssues: Int?
  var defaultBranch: String?
  var createdAt: Date?
  var updatedAt: Date?
  var pushedAt: Date?
  
  required init(map: Map) {
    
  }
  
  func mapping(map: Map) {
    id                <- map["id"]
    name              <- map["name"]
    fullName         <- map["full_name"]
    owner             <- map["owner"]
    isPrivate        <- map["private"]
    htmlUrl          <- map["html_url"]
    description       <- map["description"]
    fork              <- map["fork"]
    forks             <- map["forks"]
    url               <- map["url"]
    forksUrl         <- map["forks_url"]
    keysUrl          <- map["keys_url"]
    collaboratorsUrl <- map["collaborators_url"]
    teamsUrl         <- map["teams_url"]
    hooksUrl         <- map["hooks_url"]
    issueEventsUrl  <- map["issue_events_url"]
    eventsUrl        <- map["events_url"]
    assigneesUrl     <- map["assignees_url"]
    branchesUrl      <- map["branches_url"]
    tagsUrl          <- map["tags_url"]
    blobsUrl         <- map["blobs_url"]
    gitTagsUrl      <- map["git_tags_url"]
    gitRefsUrl      <- map["git_refs_url"]
    treesUrl         <- map["trees_url"]
    statusesUrl      <- map["statuses_url"]
    languagesUrl     <- map["languages_url"]
    stargazersUrl    <- map["stargazers_url"]
    contributorsUrl  <- map["contributors_url"]
    subscribersUrl   <- map["subscribers_url"]
    subscriptionUrl  <- map["subscription_url"]
    commitsUrl       <- map["commits_url"]
    gitCommitsUrl   <- map["git_commits_url"]
    commentsUrl      <- map["comments_url"]
    issueCommentUrl <- map["issue_comment_url"]
    contentsUrl      <- map["contents_url"]
    compareUrl       <- map["compare_url"]
    mergesUrl        <- map["merges_url"]
    archiveUrl       <- map["archive_url"]
    downloadsUrl     <- map["downloads_url"]
    issuesUrl        <- map["issues_url"]
    pullsUrl         <- map["pulls_url"]
    milestonesUrl    <- map["milestones_url"]
    notificationsUrl <- map["notifications_url"]
    labelsUrl        <- map["labels_url"]
    releasesUrl      <- map["releases_url"]
    deploymentsUrl   <- map["deployments_url"]
    gitUrl           <- map["git_url"]
    sshUrl           <- map["ssh_url"]
    cloneUrl         <- map["clone_url"]
    svnUrl           <- map["svn_url"]
    homepage          <- map["homepage"]
    size              <- map["size"]
    stargazersCount  <- map["stargazers_count"]
    watchersCount    <- map["watchers_count"]
    language          <- map["language"]
    hasIssues        <- map["has_issues"]
    hasProjects      <- map["has_projects"]
    hasDownloads     <- map["has_downloads"]
    hasWiki          <- map["has_wiki"]
    hasPages         <- map["has_pages"]
    forksCount       <- map["forks_count"]
    mirrorUrl        <- map["mirror_url"]
    openIssuesCount <- map["open_issues_count"]
    openIssues       <- map["open_issues"]
    defaultBranch    <- map["default_branch"]
    score             <- map["score"]
    watchers          <- map["watchers"]
    createdAt        <- map["created_at"]
    updatedAt        <- map["updated_at"]
    pushedAt         <- map["pushed_at"]
  }
  
}
