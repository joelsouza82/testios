//
//  FeedRepositoriesTableViewCell.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import UIKit
import SDWebImage

class FeedRepositoriesTableViewCell: UITableViewCell {
  
  @IBOutlet weak var repoName: UILabel!
  @IBOutlet weak var repoDescription: UILabel!
  @IBOutlet weak var numForks: UILabel!
  @IBOutlet weak var numStar: UILabel!
  @IBOutlet weak var userPicture: UIImageView!
  @IBOutlet weak var userFullName: UILabel!
  @IBOutlet weak var userName: UILabel!
 
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  //Tratar imagens
  func loadCellForData(repositories: Repositories) {
    repoName.text = repositories.name
    userFullName.text = repositories.fullName?.fullName()
    userName.text = repositories.owner?.login
    repoDescription.text = repositories.description
    numForks.text = repositories.forksCount?.toString()
    numStar.text = repositories.stargazersCount?.toString()
    
    SDImageCache.shared().shouldCacheImagesInMemory = false
    userPicture.sd_setHighlightedImage(with: URL(string: (repositories.owner?.avatarUrl)!)!, options: SDWebImageOptions(rawValue: 0)) { (image, error, cacheType, url) in
      if ((image) != nil){
        self.userPicture.image = image
      }
    }
    
  }

}
