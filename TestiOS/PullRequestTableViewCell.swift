//
//  PullRequestTableViewCell.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import UIKit
import SDWebImage

class PullRequestTableViewCell: UITableViewCell {
    
 @IBOutlet weak var authorPicture: UIImageView!
 @IBOutlet weak var authorUsername: UILabel!
 @IBOutlet weak var authorFullName: UILabel!
 @IBOutlet weak var pullTitle: UILabel!
 @IBOutlet weak var pullBody: UILabel!
  
  
 override func awakeFromNib() {
    super.awakeFromNib()
    
 }

 override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
 }
  
  func loadCell(pullObjective: PullRequest) {
    pullTitle.text = pullObjective.title
    pullBody.text  = pullObjective.body
    
    authorUsername.text = pullObjective.user?.login
    authorFullName.text = pullObjective.createdAt?.stringDate()
    
    SDImageCache.shared().shouldCacheImagesInMemory = false
    
    authorPicture.sd_setHighlightedImage(with: URL(string: (pullObjective.user?.avatarUrl)!)!, options: SDWebImageOptions.continueInBackground) { (image, error, cacheType, url) in
      if ((image) != nil){
        self.authorPicture.image = image
      }
    }
    
  }
  
  
}
