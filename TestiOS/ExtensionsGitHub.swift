//
//  ExtensionsGitHub.swift
//
//  Created by Joel de Almeida Souza on 23/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import Foundation


extension String{
  func fullName() -> String {
    let arrayString = self.components(separatedBy: "/")
    return "\(arrayString[0]) \(arrayString[1])"
  }
  
  func stringDate() -> String? {
    
    let dateFormatter = DateFormatter()
    let locale = dateFormatter.locale
    
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    
    if let date = dateFormatter.date(from: self){
      dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
      dateFormatter.locale = locale
      return dateFormatter.string(from: date)
    }
    return nil
  }
  
}

extension Int {
    func toString() -> String {
        return String(self)
    }
}
