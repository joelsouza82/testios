//
//  HeaderPullDetailTViewController.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import UIKit

class HeaderPullDetailTViewController: UITableViewCell {

  @IBOutlet weak var num_opened: UILabel!
  @IBOutlet weak var num_closed: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
