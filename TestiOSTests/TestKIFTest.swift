//
//  TestKIFTest.swift
//
//  Created by Joel de Almeida Souza on 24/04/18.
//  Copyright © 2018 Joel. All rights reserved.
//

import XCTest
@testable import TestiOS

import KIF

class TestKIFTest: KIFTestCase {
  
  var navigationC = UINavigationController()
  
  override func tearDown() {
  // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
    
  override func setUp() {
    super.setUp()
    
  }
  
  func testExample() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
  }
  
  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measure {
      // Put the code you want to measure the time of here.
    }
  }
  
  func testClickFeedCell_ShowPullSelectDetail() {
    tester().tapView(withAccessibilityLabel: "feedCell")
    tester().waitForView(withAccessibilityLabel: "detailCell")
  }
  
  
  
  
}
